# 1BT

Testowe repozytorium klasy 1AT Technikum Programistycznego InfoTech

## Zadanie na 6

## Kroki do wykonania

- sklonuj to repozytorium u siebie na dysku. 
    
    Linki do repozytorium: 
    - https: [https://gitlab.com/dariusz.matysiuk/1bt.git](https://gitlab.com/dariusz.matysiuk/1bt.git)
    - ssh: [git@gitlab.com:dariusz.matysiuk/1bt.git](git@gitlab.com:dariusz.matysiuk/1bt.git)

- utwórz plik twoje imię i nazwisko .txt oddzielając imię od nazwiska znakiem "_" (np.: dariusz_matysiuk.txt)
- w pliku napisz dlaczego zasługujesz na 6 :-)
- zrób commit
- wypchnij zmiany do repozytorium zdalnego

## Podsumowanie

Osoby których pliki z ich imieniem i nazwiskiem znajdę w repozytorium do 25.11.2022 otrzymają 6 z tego zadania.


> Potrzebne komendy znajdziecie w opisie lekcji na classroom'ie lub w dokumentacji GIT'a

